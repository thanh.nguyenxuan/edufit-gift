const charSet = [];
const numSet = [];

for (var i=65; i<=90; i++) charSet.push(String.fromCharCode(i));
for (var i=48; i<=57; i++) numSet.push(String.fromCharCode(i));

function generateRandStr()
{
    var nameStr = '';
    var codeStr = '';
    var nameLength = 15;
    var codeLength = 8;
    var preCharLength = 2;

    for (var i=1; i<=nameLength; i++){
        if(i<=preCharLength){
            nameStr+= randomChar();
        }else{
            nameStr+= randomNum();
        }
    }

    for (var i=1; i<=codeLength; i++){
        if(i<=preCharLength){
            codeStr+= randomChar();
        }else{
            codeStr+= randomNum();
        }
    }

    return nameStr+' - '+codeStr;
}

function randomChar(){
    return charSet[Math.floor(Math.random()*charSet.length)];
}

function randomNum(){
    return numSet[Math.floor(Math.random()*numSet.length)];
}


function applyMatrix(elementIdentify)
{
    $(elementIdentify).each(function () {
        var element = $(this);
        setInterval(function () {
            element.html(generateRandStr());
        }, 100);
    });
}

