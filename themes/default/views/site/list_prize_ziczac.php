<?php
/**
 * @var $this SiteController
 * @var $item  WPrize
 */

?>

<div class="main_action">
    <a class="btn btn-lg btn-default btnBack" href="<?php echo Yii::app()->createUrl('site/index')?>"><i class="fa fa-chevron-left"></i></a>
    <br/>
    <a class="btn btn-lg btn-default btnReport" href="<?php echo Yii::app()->createUrl('site/report')?>"><i class="fa fa-file-text"></i></a>
</div>

<h2 class="prize_name">Danh sách giải thưởng</h2>
<div class="space_50"></div>


<div class="list_prize">

    <?php
    $model = new WPrize();
    $data = $model->search(FALSE);

    $total = count($data) - 1;
    $ziczac = ($total >= 4 && $total%3 != 0);
    $pre_col = '<div class="col-xs-2"></div>';
    $odd = FALSE;
    $row = 0;
    $item_per_row = 3;
    $item_per_row_count = 0;
    $row_open_tag = '<div class="row">';
    $row_close_tag = '</div>';
    $col_class = 'col-xs-4';
    $special_pre_col = '<div class="col-xs-4"></div>';

    foreach ($data as $item){
//        if($item_per_row_count == 0){
//            echo $row_open_tag;
//        }
//        if(in_array($item->level, [1])){
//            $item_pre_html = "<div class='{$col_class}'></div>";
//            $item_per_row_count = $item_per_row;
//        }else{
//            $item_pre_html = '';
//            $item_per_row_count++;
//        }
//        echo $item_pre_html;

        if($item->level == 1){
            echo $row_open_tag;
            $row++;
            echo $special_pre_col;
        }else{
            if($item_per_row_count == 0){
                echo $row_open_tag;
                $row++;
                if($row % 2 == 0){
                    $odd = TRUE;
                }else{
                    $odd = false;
                }
                if($odd && $ziczac){
                    echo $pre_col;
                }
            }
            if($ziczac){
                if($odd){
                    $item_per_row = 2;
                }else{
                    $item_per_row = 3;
                }
            }
            $item_per_row_count++;
        }
    ?>

    <div class="<?php echo $col_class?>">
        <div class="item_prize"
             data-url="<?php echo Yii::app()->createUrl('site/prize', ['id' => $item->id])?>"
             data-status="<?php echo $item->status?>">
            <div class="item_bg noselect">
                <img src="<?php echo Yii::app()->theme->baseUrl?>/images/landingpage/background_button.svg" />
            </div>
            <div class="item_content">
                <div class="item_thumbnail noselect">
                    <img src="<?php echo $item->image?>" />
                </div>
                <div class="item_title">
                    <?php echo $item->name?>
                    <span><?php echo (($item->slot > 1) ? ($item->slot .' ') : '') . $item->gifts[0]->name ?></php></span>
                </div>
                <div class="item_price">
                    <?php echo Utils::formatCurrency($item->price)?>
                </div>
            </div>

            <?php if($item->status == WPrize::STATUS_INACTIVE){ ?>
            <div class="item_check">
                <img src="<?php echo Yii::app()->theme->baseUrl?>/images/landingpage/check.png" />
            </div>
            <?php }?>
        </div>
    </div>

    <?php
        if($item->level == 1){
            echo $special_pre_col;
            echo $row_close_tag;
        }else{
            if($item_per_row_count == $item_per_row){
                $item_per_row_count = 0;
                if($odd && $ziczac){
                    echo $pre_col;
                }
                echo $row_close_tag;
            }
        }
    }
    ?>

</div>

<script>
    $(document).ready(function () {
        $('.item_prize').on('click', function(){
            //if($(this).data('status') == '<?php //echo WPrize::STATUS_ACTIVE?>//'){
                window.location.href = $(this).data('url');
            // }else{
            //     alertPrizeInactive();
            // }
        });
    });

    $(window).scroll(function () {
        //set scroll position in session storage
        sessionStorage.scrollPos = $(window).scrollTop();
    });
    var initPosition = function () {
        //return scroll position in session storage
        $(window).scrollTop(sessionStorage.scrollPos || 0)
    };
    window.onload = initPosition;
</script>