<?php
/**
* @var $this SiteController
* @var $data WPrize[]
*/

?>

<div class="main_action">
    <a class="btn btn-lg btn-default btnBack" href="<?php echo Yii::app()->createUrl('site/listPrize')?>"><i class="fa fa-chevron-left"></i></a>
</div>

<h2 class="prize_name">Danh sách trúng thưởng</h2>


<div class="x_panel">

    <div class="x_title">
        <div class="pull-right">
            <a href="<?php echo Yii::app()->createUrl('site/exportWinner')?>" class="btn btn-success" download><i class="fa fa-file-excel-o"></i> Excel</a>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <table id="tableReportWinners" class="table table-responsive table-striped table-bordered table-hover table-sticky-header">
            <thead>
            <tr>
                <th>Ảnh giải thưởng</th>
                <th>Tên giải thưởng</th>
                <th>Giá trị (VND)</th>
                <th>Số lượng</th>
                <th>Người trúng</th>
                <th>Mã</th>
<!--                <th>Chức vụ</th>-->
                <th>Phòng ban</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $prize){
                $row_span = count($prize->gifts);
                echo "<tr>";
                echo "<td rowspan='{$row_span}' class='text-center'><img src='{$prize->image}'/></></td>";
                echo "<td rowspan='{$row_span}'>" .
                    "<b>{$prize->name}</b>" .
//                    "<br/>" .
//                    "{$prize->gifts[0]->name}" .
                    "</td>";
                echo "<td rowspan='{$row_span}' class='text-right'>".Utils::formatCurrency($prize->gifts[0]->price, '')."</td>";
                echo "<td rowspan='{$row_span}'>{$prize->slot}</td>";
                $row_open = TRUE;
                foreach ($prize->gifts as $gift){
                    $hasWinner = !empty($gift->gift_player);
                    if(!$row_open) {
                        echo "<tr>";
                    }
                    echo "<td>".($hasWinner ? $gift->gift_player->player->name : '')."</td>";
                    echo "<td><b>".($hasWinner ? $gift->gift_player->player->code : '')."</b></td>";
//                    echo "<td>".($hasWinner ? $gift->gift_player->player->position : '')."</td>";
                    echo "<td>".($hasWinner ? $gift->gift_player->player->department : '')."</td>";
                    echo "</tr>";
                    $row_open = FALSE;
                }
            }
            ?>
            </tbody>
        </table>
    </div>

</div>
