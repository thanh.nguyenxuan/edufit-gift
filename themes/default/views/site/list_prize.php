<?php
/**
 * @var $this SiteController
 * @var $item  WPrize
 */
$model = new WPrize();
$data = $model->search(FALSE);
$default_active_prize = !empty($_COOKIE['active_prize']) ? $_COOKIE['active_prize'] : null;
$navigationTooltips = [];
$i=0;
foreach ($data as $item){
    $navigationTooltips[] = '#'.(++$i).' '.$item->name;
}
?>

<div class="main_action">
    <a class="btn btn-lg btn-default btnBack" href="<?php echo Yii::app()->createUrl('site/index')?>"><i class="fa fa-chevron-left"></i></a>
    <br/>
    <a class="btn btn-lg btn-default btnReport" href="<?php echo Yii::app()->createUrl('site/report')?>"><i class="fa fa-file-text"></i></a>
</div>

<div id="fullpage" class="list_prize">

    <?php foreach ($data as $item){ ?>
        <div class="section <?php echo ($default_active_prize == $item->id) ? 'active' : ''?>">
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4">
                    <div class="item_prize"
                         data-id="<?php echo $item->id?>"
                         data-url="<?php echo Yii::app()->createUrl('site/prize', ['id' => $item->id])?>"
                         data-status="<?php echo $item->status?>">
                        <div class="item_bg noselect">
                            <img src="<?php echo Yii::app()->theme->baseUrl?>/images/landingpage/background_button.svg" />
                        </div>
                        <div class="item_content">
                            <div class="item_thumbnail noselect">
                                <img src="<?php echo $item->image?>" />
                            </div>
                            <div class="item_title">
                                <?php echo $item->name?>
                                <span><?php echo $item->getDescription()?></span>
                            </div>
<!--                            <div class="item_price">-->
<!--                                --><?php //echo Utils::formatCurrency($item->price)?>
<!--                            </div>-->
                        </div>

                        <?php if($item->status == WPrize::STATUS_INACTIVE){ ?>
                            <div class="item_check">
                                <img src="<?php echo Yii::app()->theme->baseUrl?>/images/landingpage/check.png" />
                            </div>
                        <?php }?>
                    </div>
                </div>
                <div class="col-sm-4"></div>
            </div>
        </div>
    <?php } ?>

</div>

<script>

    $(document).ready(function() {
        $('.item_prize').on('click', function(){
            window.location.href = $(this).data('url');
        });

        $('#fullpage').fullpage({
            navigation:true,
            navigationTooltips: <?php echo json_encode($navigationTooltips)?>,
            showActiveTooltip: false,
            onLeave:function(origin, destination, direction){
                setCookie('active_prize', ($('.item_prize').eq(destination.index).data('id')), 1);
            },
        });
    });

    $(document).on('keypress', function(e){
        if(e.charCode === 13){
            $('.list_prize .section.active .item_prize').click();
        }
        if(e.charCode === 112){
            window.location.href = $('.btnReport').attr('href');
        }
    });


</script>