<?php
/**
 * @var $this SiteController
 * @var $model WPrize
 * @var $item WGift
 */

?>

<div class="main_action">
    <a class="btn btn-lg btn-default btnBack" href="<?php echo Yii::app()->createUrl('site/listPrize')?>"><i class="fa fa-chevron-left"></i></a>
</div>

<a class="btn btn-lg btn-warning btnSpinPrize <?php echo ($model->status == WPrize::STATUS_INACTIVE) ? 'disabled hidden' : ''?>" onclick="spinThePrize('<?php echo $model->id?>')">Quay!</a>

<h2 class="prize_name">
    <?php echo $model->name?>
</h2>

<div class="space_50"></div>

<div class="list_gift">

    <?php
    $data = $model->gifts;

    $total = count($data);
    $ziczac = ($total >= 4 && $total%3 != 0 );
    $pre_col = ($ziczac) ? '<div class="col-xs-2"></div>' : '';
    $row=0;
    $odd = FALSE;
    $item_per_row_count = 0;
    $row_open_tag = '<div class="row">';
    $row_close_tag = '</div>';

    switch (count($data)){
        case 1:
            $item_per_row = 1;
            $col_class = 'col-xs-12';
            break;
        case 2:
            $item_per_row = 2;
            $col_class = 'col-xs-4';
            $ziczac = TRUE;
            $row=1;
            $pre_col = '<div class="col-xs-2"></div>';
            break;
        default:
            $item_per_row = 3;
            $col_class = 'col-xs-4';
    }

    foreach ($data as $item){
        if($item_per_row_count == 0){
            echo $row_open_tag;
            $row++;
            if ($total == 7) {
                if($row % 2 != 0){
                    $odd = TRUE;
                }else{
                    $odd = false;
                }
                if($odd && $ziczac){
                    echo $pre_col;
                }
            } else {
                if($row % 2 == 0){
                    $odd = TRUE;
                }else{
                    $odd = false;
                }
                if($odd && $ziczac){
                    echo $pre_col;
                }
            }

        }
        if($ziczac){
            if($odd){
                $item_per_row = 2;
            }else{
                $item_per_row = 3;
            }
        }
        $item_per_row_count++;
        $hasWinner = !empty($item->gift_player);
        $winnerId = !empty($item->gift_player) ? $item->gift_player->player_id : '';

        ?>

        <div class="<?php echo $col_class?>">
            <div class="item_gift"
                 data-id="<?php echo $item->id?>"
                 data-url="<?php echo Yii::app()->createUrl('site/prize', ['id' => $item->id])?>"
                 data-status="<?php echo $item->status?>"
                 data-winder-id="<?php echo $winnerId?>">
                <div class="item_bg noselect">
                    <img src="<?php echo Yii::app()->theme->baseUrl?>/images/landingpage/background_button.svg" />
                </div>
                <div class="item_content">
                    <div class="item_thumbnail noselect">
                        <img src="<?php echo $item->image?>" />
                        <div class="check">
                            <!--<input class="flat" type="checkbox">-->
                        </div>
                    </div>
                    <div class="item_winner">
                        <div class="name <?php echo ($hasWinner) ? '' : 'hidden'?>">
                            <?php echo ($hasWinner) ? $item->getWinnerName() : ''?>
                        </div>
                        <div class="description <?php echo ($hasWinner) ? '' : 'hidden'?>">
                            <?php echo ($hasWinner) ? $item->getWinnerDepartment() : ''?>
                        </div>
                        <div class="default <?php echo ($hasWinner) ? 'hidden' : ''?>">
                            -
                        </div>
                        <div class="matrix hidden"></div>
                    </div>
                    <div class="action <?php echo ($hasWinner) ? '' : 'hidden'?>">
                        <a class="btn btn-danger btn-lg btnNotReceive" onclick="confirmNotReceive('<?php echo $item->id?>')">Ứ lên nhận quà</a>
                    </div>
                </div>
            </div>
        </div>

        <?php
        if($item_per_row_count == $item_per_row){
            $item_per_row_count = 0;
            if($odd && $ziczac){
                echo $pre_col;
            }
            echo $row_close_tag;
        }
    }
    ?>
</div>

<div id="modalConfirmNotReceive" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-body">
                <div class="image">
                    <img src="<?php echo Yii::app()->theme->baseUrl?>/images/2021/question.gif"/>
                </div>
                <div class="message">

                </div>
            </div>
            <div class="modal-footer">
                <button id="btnConfirmNotReceive" data-gift-id="" type="button" class="btn btn-danger disabled">Chê</button>
            </div>
        </div>

    </div>
</div>

<script>

    var modalConfirmNotReceive = $('#modalConfirmNotReceive');
    var btnConfirmNotReceive = $('#btnConfirmNotReceive');
    var btnSpinPrize = $('.btnSpinPrize');

    var themeAudio = new Audio('<?php echo Yii::app()->theme->baseUrl?>/audios/music.mp3');
    var congratAudio = new Audio('<?php echo Yii::app()->theme->baseUrl?>/audios/congratulation.mp3');
    themeAudio.loop = true;
    themeAudio.volume = 0.2;

    function spinThePrize(prize_id)
    {
        if(btnSpinPrize.hasClass('disabled')){
            return;
        }
        btnSpinPrize.addClass('disabled');
        themeAudio.volume = 0.2;
        themeAudio.play();

        $('.item_gift').each(function () {
            if($(this).data('winder-id') == ''){
                $(this).find('.item_winner .name').addClass('hidden');
                $(this).find('.item_winner .description').addClass('hidden');
                $(this).find('.item_winner .default').addClass('hidden');
                $(this).find('.item_winner .matrix').removeClass('hidden');
            }
        });

        $.ajax({
            url : '<?php echo Yii::app()->createUrl('api/spinThePrize')?>',
            type : 'POST',
            dataType: 'json',
            data : {
                YII_CSRF_TOKEN : '<?php echo Yii::app()->request->csrfToken?>',
                prize_id : prize_id
            },
            success: function(response){
                if(response.success){
                    setTimeout(function(){ showWinners(response) }, 7*1000);
                }else{
                    alertSystemError();
                }
            }
        });
    }

    function showWinners(response)
    {
        response.data.gifts.forEach(function (item, index, arr) {
            var winnerName = item.winner.name + ' - ' + item.winner.code;
            // var winnerDes = item.winner.position + ' - ' + item.winner.department;
            var winnerDes = item.winner.department;
            var gift = $('.item_gift[data-id="'+item.id+'"]');

            gift.find('.item_winner .name').html(winnerName);
            gift.find('.item_winner .description').html(winnerDes);
            gift.data('winder-id', item.winner.id);

            gift.find('.item_winner .name').removeClass('hidden');
            gift.find('.item_winner .description').removeClass('hidden');
            gift.find('.item_winner .default').addClass('hidden');
            gift.find('.item_winner .matrix').addClass('hidden');
            gift.find('.action').removeClass('hidden');
        });

        btnSpinPrize.addClass('hidden');
        startConfetti();
        congratAudio.play();

        setTimeout(function(){ themeAudio.volume=0.175; }, 500);
        setTimeout(function(){ themeAudio.volume=0.150; }, 1000);
        setTimeout(function(){ themeAudio.volume=0.125; }, 1.5*1000);
        setTimeout(function(){ themeAudio.volume=0.100; }, 2*1000);
        setTimeout(function(){ themeAudio.volume=0.075; }, 3*1000);
        setTimeout(function(){ themeAudio.volume=0.050; }, 4*1000);
        setTimeout(function(){ themeAudio.volume=0.025; }, 5*1000);
        setTimeout(function(){ themeAudio.volume=0.0; themeAudio.stop(); }, 6*1000);

        setTimeout(function(){ stopConfetti(); }, 5*1000);
    }

    function notReceive(gift_id)
    {
        var gift = $('.item_gift[data-id="'+gift_id+'"]');
        var user_id = gift.data('winder-id');
        var btn = gift.find('.btnNotReceive');
        if(btn.hasClass('disabled')){
            return;
        }
        btn.addClass('disabled');

        $.ajax({
            url : '<?php echo Yii::app()->createUrl('api/userNotReceivePrize')?>',
            type : 'POST',
            dataType: 'json',
            data : {
                YII_CSRF_TOKEN : '<?php echo Yii::app()->request->csrfToken?>',
                user_id : user_id
            },
            success: function(response){
                if(response.success){
                    gift.data('winder-id', '');
                    gift.find('.item_winner .name').html('').addClass('hidden');
                    gift.find('.item_winner .description').html('').addClass('hidden');
                    gift.find('.item_winner .default').removeClass('hidden');
                    gift.find('.action').addClass('hidden');
                    btn.removeClass('disabled');
                    btnSpinPrize.removeClass('disabled').removeClass('hidden');
                }else{
                    alertSystemError();
                }
            }
        });

    }

    function confirmNotReceive(gift_id) {
        var gift = $('.item_gift[data-id="'+gift_id+'"]');
        var winner = gift.find('.item_winner .name').text().trim();
        var message = '"'+winner+'" chê quà?';

        modalConfirmNotReceive.find('.message').text(message);
        btnConfirmNotReceive.data('gift-id', gift_id).removeClass('disabled');
        modalConfirmNotReceive.modal('show');
    }

    $(document).ready(function(){
        applyMatrix('.item_gift .item_winner .matrix');

        btnConfirmNotReceive.on('click', function(e){
            var btn = $(this);
            if(btn.hasClass('disabled')){
                return;
            }
            var gift_id = $(this).data('gift-id');
            if(gift_id){
                btn.addClass('disabled');
                notReceive(gift_id);
                modalConfirmNotReceive.modal('hide');
            }else{
                alertSystemError();
            }
        });

        var showBtnNotReceiveTimeout;
        var showBtnNotReceive = false;
        $(".item_gift")
            .on('mousedown', function(e){
                if(event.which === 1){
                    var target = $(this).find('.action');
                    if(!showBtnNotReceive){
                        showBtnNotReceiveTimeout = setTimeout(function (){
                            target.css('display','block');
                            showBtnNotReceive = true;
                        }, 200);
                    }
                }
            })
            .on('mouseleave', function(e){
                $(this).find('.action').css('display','none');
                clearTimeout(showBtnNotReceiveTimeout);
                showBtnNotReceive = false;
            })
        ;


        // $('input.flat').iCheck({
        //     checkboxClass: 'icheckbox_flat-red',
        //     radioClass: 'iradio_flat-red'
        // });

    });

    $(document).on('keypress', function(e){
        if(e.charCode === 13){
            btnSpinPrize.click();
        }
    });

</script>




