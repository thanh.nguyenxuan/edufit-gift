<?php
$this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Change Password");
$this->breadcrumbs = array(
    UserModule::t("Profile") => array('/user/profile'),
    UserModule::t("Change password"),
);
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?php echo UserModule::t("Change password"); ?></h2>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <?php //echo $this->renderPartial('menu');  ?>

        <div class="form">
            <?php
            $form = $this->beginWidget('UActiveForm', array(
                'id' => 'changepassword-form',
                'enableAjaxValidation' => true,
                'htmlOptions' => array(
                    'class' => 'form-horizontal form-label-left',
                ),
            ));
            ?>

            <?php echo Yii::t('web/form','require_fields')?>

            <?php echo CHtml::errorSummary($model); ?>

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'oldPassword'); ?>
                        <?php echo $form->passwordField($model, 'oldPassword', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'oldPassword'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'password'); ?>
                        <?php echo $form->passwordField($model, 'password', array(
                            'class' => 'form-control',
                            'placeholder' => UserModule::t("Minimal password length 4 symbols.")
                        )); ?>
                        <?php echo $form->error($model, 'password'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'verifyPassword'); ?>
                        <?php echo $form->passwordField($model, 'verifyPassword', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'verifyPassword'); ?>
                    </div>
                </div>
            </div>

            <div class="ln_solid"></div>

            <?php echo CHtml::submitButton(UserModule::t("Save"), array('class' => 'btn btn-success')); ?>

            <?php $this->endWidget(); ?>
        </div><!-- form -->
    </div>
</div>
