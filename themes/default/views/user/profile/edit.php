<?php
/**
 * @var $this ProfileController
 * @var $model User
 * @var $profile
 * @var $form UActiveForm
 */
$this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Profile");
$this->breadcrumbs = array(
    UserModule::t("Profile") => array('profile'),
    UserModule::t("Edit"),
);
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo UserModule::t('Edit profile'); ?></h2>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <?php //echo $this->renderPartial('menu');  ?>

        <?php if (Yii::app()->user->hasFlash('profileMessage')): ?>
            <div class="alert alert-success alert-dismissible fade in">
                <button aria-label="<?php echo Yii::t('app', 'Close') ?>" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
                <?php echo Yii::app()->user->getFlash('profileMessage'); ?>
            </div>
        <?php endif; ?>
        <div class="form">
            <?php
            $form = $this->beginWidget('UActiveForm', array(
                'id' => 'profile-form',
                'enableAjaxValidation' => true,
                'htmlOptions' => array(
                    'class' => 'form-horizontal form-label-left',
                    'enctype' => 'multipart/form-data',
                ),
            ));
            ?>

            <?php echo Yii::t('web/form', 'require_fields')?>

            <?php echo $form->errorSummary($model); ?>

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'name'); ?>
                        <?php echo $form->textField($model, 'name', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'name'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'email'); ?>
                        <?php echo $form->textField($model, 'email', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'email'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'phone'); ?>
                        <?php echo $form->textField($model, 'phone', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'phone'); ?>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <?php echo $this->renderPartial('//layouts/_upload_form', array(
                            'form' => $form,
                            'model' => $model,
                            'attribute' => 'avatar',
                            'upload_url' => Yii::app()->createUrl('user/profile/upload'),
                            'accept' => 'image/*',
                        ))?>
                    </div>
                </div>
            </div>

            <div class="ln_solid"></div>

            <?php echo CHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'), array('class' => 'btn btn-success')); ?>

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>
</div>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar')
                    .attr('src', e.target.result)
                    .removeClass('hidden');
            };

            reader.readAsDataURL(input.files[0]);
        }else{
            $('#avatar').addClass('hidden');
        }
    }
</script>