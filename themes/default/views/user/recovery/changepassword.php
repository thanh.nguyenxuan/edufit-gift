<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change password");
$this->breadcrumbs=array(
	UserModule::t("Login") => array('/user/login'),
	UserModule::t("Change password"),
);
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?php echo UserModule::t("Change password"); ?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="form">
            <?php echo CHtml::beginForm(); ?>

            <?php echo Yii::t('web/form','require_fields');?>
            <?php echo CHtml::errorSummary($form); ?>

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <?php echo CHtml::activeLabelEx($form,'password'); ?>
                        <?php echo CHtml::activePasswordField($form,'password', array(
                            'class' => 'form-control',
                            'placeholder' => UserModule::t("Minimal password length 4 symbols.")
                        )); ?>
                    </div>

                    <div class="form-group">
                        <?php echo CHtml::activeLabelEx($form,'verifyPassword'); ?>
                        <?php echo CHtml::activePasswordField($form,'verifyPassword', array('class' => 'form-control')); ?>
                    </div>
                </div>
            </div>

            <div class="ln_solid"></div>

            <?php echo CHtml::submitButton(UserModule::t("Save"), array('class' => 'btn btn-success')); ?>

            <?php echo CHtml::endForm(); ?>
        </div><!-- form -->
    </div>
</div>

