<?php
$this->breadcrumbs = array(
    UserModule::t('Manage Users') => array('admin'),
    Yii::t('web/form','Update'),
);
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo UserModule::t('Update User') . " \"" . $model->username ."\""?></h2>
                <div class="clearfix"></div>
            </div>                

            <div class="x_content">
                <?php
                echo $this->renderPartial('_form', array('model' => $model));
                ?>
            </div>
        </div>
    </div>
</div>