<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo Yii::app()->theme->baseUrl?>/images/favicon.ico" type="image/ico">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/iCheck/skins/flat/red.css" rel="stylesheet">
    <!-- Toast -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/jquery-toast/jquery.toast.min.css" rel="stylesheet">
    <!-- FullPage -->
    <link href="<?php echo Yii::app()->baseUrl?>/vendors/fullpage/dist/fullpage.min.css" rel="stylesheet">
    <!-- Money Font -->
    <style>
        @font-face {
            font-family: 'Cash Currency';
            font-style: normal;
            font-weight: 400;
            src: local('Cash Currency'), url('https://fonts.cdnfonts.com/s/15060/Cash Currency.woff') format('woff');
        }
    </style>
    <!-- Custom Theme Style -->
    <link href="<?php echo Yii::app()->theme->baseUrl?>/css/theme.css?v=<?php echo APP_VERSION?>" rel="stylesheet">
    <link href="<?php echo Yii::app()->theme->baseUrl?>/css/landingpage.css?v=<?php echo APP_VERSION?>" rel="stylesheet">
    <link href="<?php echo Yii::app()->theme->baseUrl?>/css/custom.css?v=<?php echo APP_VERSION?>" rel="stylesheet">

    <?php if(Yii::app()->controller->id == 'site' && Yii::app()->controller->action->id == 'index'){ ?>
    <style>
        #main-content{
            background-image: url("<?php echo Yii::app()->theme->baseUrl?>/images/2024/Homepage.png");
        }
    </style>
    <?php } ?>

    <script>
        $(document).ready(function(){
            $('body').css('height', $(window).height()-1);
        });
        $(window).resize(function(){
            $('body').css('height', $(window).height()-1);
        });
    </script>

</head>

<body>
<div id="main-content">
    <div class="container-fluid">
        <?php echo $content?>
    </div>
</div>

<?php if(!(Yii::app()->controller->id == 'site' && Yii::app()->controller->action->id == 'index')){ ?>
<!--<div class="hint">-->
<!--    <p>-->
<!--        Hình ảnh chỉ mang tính chất mình họa-->
<!--        <br/>-->
<!--        Giá trị công bố của các giải thưởng có thể có chênh lệch nhỏ so với giả trị thực tế khi mua hàng-->
<!--    </p>-->
<!--</div>-->
<!---->
<!--<div class="hint right">-->
<!--    <p>-->
<!--        Images are for illustration purposes only-->
<!--        <br/>-->
<!--        The advertised value of the prizes may differ slightly from the actual value of the purchase-->
<!--    </p>-->
<!--</div>-->
<?php } ?>

<!-- iCheck -->
<script src="<?php echo Yii::app()->baseUrl?>/vendors/iCheck/icheck.min.js"></script>
<!-- Toast -->
<script src="<?php echo Yii::app()->baseUrl?>/vendors/jquery-toast/jquery.toast.min.js"></script>
<!-- FullPage -->
<script src="<?php echo Yii::app()->baseUrl?>/vendors/fullpage/dist/fullpage.min.js"></script>
<!-- Confetti -->
<script src="<?php echo Yii::app()->theme->baseUrl?>/js/confetti.js"></script>
<!-- JS -->
<script src="<?php echo Yii::app()->theme->baseUrl?>/js/matrix.js?v=<?php echo APP_VERSION?>"></script>
<script src="<?php echo Yii::app()->theme->baseUrl?>/js/alert.js?v=<?php echo APP_VERSION?>"></script>
<script src="<?php echo Yii::app()->theme->baseUrl?>/js/custom.js?v=<?php echo APP_VERSION?>"></script>

</body>
</html>
