<?php
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
?>

<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainMenu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('landingPage/index')?>">
                <img id="logo" src="<?php echo Yii::app()->theme->baseUrl?>/images/logo_edufit.png" alt="Logo"/>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="mainMenu">
            <ul class="nav navbar-nav">
                <li class="<?php echo ($controller == 'landingPage' && $action == 'index') ? 'active' : ''?>">
                    <a href="<?php echo Yii::app()->createUrl('landingPage/index')?>"><?php echo Yii::t('web/label','Home')?></a>
                </li>
                <li class="<?php echo ($controller == 'landingPage' && $action == 'policy') ? 'active' : ''?>">
                    <a href="<?php echo Yii::app()->createUrl('landingPage/policy')?>"><?php echo Yii::t('web/label','Policy')?></a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php echo Yii::app()->createUrl('user/login')?>"><?php echo Yii::t('web/label', 'Login')?></a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<style>
    nav ul li{
        
    }
    nav ul {
        text-align: center;
    }
</style>