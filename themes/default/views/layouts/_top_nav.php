<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <?php $this->widget('zii.widgets.CMenu', array(
                'encodeLabel' => false,
                'htmlOptions' => array(
                    'class' => 'nav navbar-nav navbar-right',
                ),
                'submenuHtmlOptions' => array(
                    'class' => 'dropdown-menu dropdown-usermenu animated fadeInDown pull-right',
                ),
                'items' => array(
                    array(
                        'url'       => Yii::app()->getModule('user')->loginUrl,
                        'label'     => '<i class="fa fa-sign-in"></i> ' . Yii::app()->getModule('user')->t("Login"),
                        'visible'   => Yii::app()->user->isGuest,
                    ),
                    array(
                        'url'       => 'javascript:;',
                        'linkOptions' => array(
                            'class'         => 'user-profile dropdown-toggle',
                            'data-toggle'   => 'dropdown',
                            'aria-expanded' => 'false',
                        ),
                        'label'     => '<img src="'.User::getAvatarUrl(Yii::app()->user->id).'" alt="">' . Yii::app()->user->name . ' <span class=" fa fa-angle-down"></span>',
                        'visible'   => !Yii::app()->user->isGuest,
                        'items'     => array(
                            array(
                                'url'   => Yii::app()->getModule('user')->profileUrl,
                                'label' => Yii::app()->getModule('user')->t("Profile"),
                            ),
//                            array(
//                                'url'   => 'javascript:void(0)',
//                                'label' => '<span class="badge bg-red pull-right">50%</span> <span>'.Yii::t('web/label','Settings').'</span>',
//                            ),
//                            array(
//                                'url'   => 'javascript:void(0)',
//                                'label' => Yii::t('web/label','Help'),
//                            ),
                            array(
                                'url'   => Yii::app()->getModule('user')->logoutUrl,
                                'label' => '<i class="fa fa-sign-out pull-right"></i> ' . Yii::app()->getModule('user')->t("Logout"),
                            ),
                        ),
                    ),
                ),
            ));?>

            <?php $this->widget('zii.widgets.CMenu', array(
                'encodeLabel' => false,
                'htmlOptions' => array(
                    'class' => 'nav navbar-nav navbar-right',
                ),
                'submenuHtmlOptions' => array(
                    'class' => 'dropdown-menu list-unstyled msg_list',
                ),
                'items' => array(
//                    array(
//                        'url' => 'javascript;:',
//                        'label' => '<i class="fa fa-envelope-o"></i> <span class="badge bg-green">6</span>',
//                        'linkOptions' => array(
//                            'class' => 'dropdown-toggle info-number',
//                            'data-toggle' => 'dropdown',
//                            'aria-expanded' => 'false',
//                        ),
//                        'visible' => !Yii::app()->user->isGuest,
//                        'items' => array(
//                            array(
//                                'url' => 'javascript:void(0)',
//                                'label' => $this->renderPartial('//layouts/_message', array(), true),
//                            ),
//                            array(
//                                'url' => 'javascript:void(0)',
//                                'label' => $this->renderPartial('//layouts/_message', array(), true),
//                            ),
//                            array(
//                                'url' => 'javascript:void(0)',
//                                'label' => '
//                                    <div class="text-center">
//                                        <strong>See All Alerts</strong>
//                                        <i class="fa fa-angle-right"></i>
//                                    </div>
//                                ',
//                            ),
//                        ),
//                    ),
                ),
            ));?>
        </nav>
    </div>
</div>
<!-- /top navigation -->