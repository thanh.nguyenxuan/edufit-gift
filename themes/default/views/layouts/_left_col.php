<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view menu_fixed">

        <div class="navbar nav_title" style="border: 0;">
            <a href="<?php echo $this->createUrl('/site/index') ?>" class="site_title">
                <span><?php echo CHtml::encode(Yii::app()->name) ?></span>
            </a>
        </div>

        <div class="clearfix"></div>

        <div class="ln_solid no-margin"></div>

        <?php if(!Yii::app()->user->isGuest){?>
        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo Yii::app()->createUrl('user/logout')?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->

        <?php } ?>
    </div>
</div>

