<?php
/**
 * @var $form TbActiveForm
 * @var $model
 * @var $items array()
 * @var $itemCssClass string
 * @var $itemGroupCssClass string
 * @var $groupOpenTag string
 * @var $groupCloseTag string
 * @var $separatorOpenTag string
 * @var $separatorCloseTag string
 */

//init
if(!isset($items)){
    $items = array('province', 'district', 'ward', 'address');
}
if(!isset($itemGroupCssClass)){
    $itemGroupCssClass = 'form-group';
}
if(!isset($itemCssClass)){
    $itemCssClass = 'form-control';
}
if(!isset($horizontal)){
    $horizontal = FALSE;
}

$groupOpenTag = (!isset($groupOpenTag) && $horizontal) ? '<div class="row">' : '';
$groupCloseTag = (!isset($groupCloseTag) && $horizontal) ? '</div>' : '';
$separatorOpenTag = (!isset($separatorOpenTag) && $horizontal) ? '<div class="col-sm-3">' : '';
$separatorCloseTag = (!isset($separatorCloseTag) && $horizontal) ? '</div>' : '';

//render
echo $groupOpenTag;
if(in_array('province', $items)){
    echo $separatorOpenTag;
    echo "<div class='$itemGroupCssClass'>";
    echo $form->labelEx($model, 'province_code');
    $this->widget( 'booster.widgets.TbSelect2', array(
        'model'         => $model,
        'attribute'     => 'province_code',
        'data'          => WProvince::getListData(),
        'htmlOptions'   => array(
            'multiple'      => FALSE,
            'prompt'        => Yii::t('web/form', 'Select'),
            'class'         => $itemCssClass,
            'style'         => 'width:100%;',
            'ajax'     => array(
                'type'     => 'POST',
                'url'      => Yii::app()->controller->createUrl('site/getListDistrictByProvince'),
                'update'   => '#'.CHtml::activeId($model,'district_code'),
                'data'     => array(
                    'province_code' => 'js:this.value',
                    'YII_CSRF_TOKEN' => Yii::app()->request->csrfToken
                ),
            ),
            'onchange' => '$("#'.CHtml::activeId($model,'district_code').'").select2("val", "");$("#'.CHtml::activeId($model,'ward_code').'").html("<option value>'.Yii::t('web/form', 'Select').'</option>").select2("val", "");'
        ),
    ));
    echo $form->error($model, 'province_code');
    echo '</div>';
    echo $separatorCloseTag;
}

if(in_array('district', $items)){
    echo $separatorOpenTag;
    echo "<div class='$itemGroupCssClass'>";
    echo $form->labelEx($model, 'district_code');
    $this->widget( 'booster.widgets.TbSelect2', array(
        'model'         => $model,
        'attribute'     => 'district_code',
        'data'          => (!empty($model->province_code)) ? WDistrict::getListData($model->province_code) : array(),
        'htmlOptions'   => array(
            'multiple'      => FALSE,
            'prompt'        => Yii::t('web/form', 'Select'),
            'class'         => $itemCssClass,
            'style'         => 'width:100%;',
            'ajax'     => array(
                'type'     => 'POST',
                'url'      => Yii::app()->controller->createUrl('site/getListWardByDistrict'),
                'update'   => '#'.CHtml::activeId($model,'ward_code'),
                'data'     => array(
                    'district_code' => 'js:this.value',
                    'YII_CSRF_TOKEN' => Yii::app()->request->csrfToken
                ),
            ),
            'onchange' => '$("#'.CHtml::activeId($model,'ward_code').'").select2("val", "");'
        ),
    ));
    echo $form->error($model, 'district_code');
    echo '</div>';
    echo $separatorCloseTag;
}

if(in_array('ward', $items)){
    echo $separatorOpenTag;
    echo "<div class='$itemGroupCssClass'>";
    echo $form->labelEx($model, 'ward_code');
    $this->widget( 'booster.widgets.TbSelect2', array(
        'model'         => $model,
        'attribute'     => 'ward_code',
        'data'          => (!empty($model->district_code)) ? WWard::getListData($model->district_code) : array(),
        'htmlOptions'   => array(
            'multiple'      => FALSE,
            'prompt'        => Yii::t('web/form', 'Select'),
            'class'         => $itemCssClass,
            'style'         => 'width:100%;',
        ),
    ));
    echo $form->error($model, 'ward_code');
    echo '</div>';
    echo $separatorCloseTag;
}

if(in_array('address', $items)){
    echo $separatorOpenTag;
    echo "<div class='$itemGroupCssClass'>";
    echo $form->labelEx($model, 'address');
    echo $form->textArea($model, 'address', array('class' => $itemCssClass));
    echo $form->error($model, 'address');
    echo '</div>';
    echo $separatorCloseTag;
}
echo $groupCloseTag;
?>