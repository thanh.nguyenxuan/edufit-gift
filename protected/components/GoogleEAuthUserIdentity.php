<?php

class GoogleEAuthUserIdentity extends CBaseUserIdentity {

	const ERROR_NOT_AUTHENTICATED = 3;
	const ERROR_EMAIL_NOT_EXIST = 4;
	const ERROR_STATUS_INACTIVE = 5;
	const ERROR_STATUS_BAN = 6;

	/**
	 * @var EAuthServiceBase the authorization service instance.
	 */
	protected $service;

	/**
	 * @var string the unique identifier for the identity.
	 */
	protected $id;

	/**
	 * @var string the display name for the identity.
	 */
	protected $name;

	/**
	 * Constructor.
	 *
	 * @param EAuthServiceBase $service the authorization service instance.
	 */
	public function __construct($service) {
		$this->service = $service;
	}

	/**
	 * Authenticates a user based on {@link service}.
	 * This method is required by {@link IUserIdentity}.
	 *
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate() {
		if ($this->service->isAuthenticated) {
//			$this->id = $this->service->id;
//			$this->name = $this->service->getAttribute('name');

			$access_key = $this->service->getAcessTokenValue();
			$url = 'https://www.googleapis.com/oauth2/v1/userinfo';

			$profile = json_decode(Utils::callAPI('GET', $url, array(), array('access_token' => $access_key)));

            $email = $profile->email;
            $user = User::model()->findByAttributes(array('email' => $email));
            if($user){
                if($user->status == User::STATUS_ACTIVE){
                    $this->id = $user->id;
                    $this->name = $user->username;

//                    $this->setState('id', $this->id);
//                    $this->setState('name', $this->name);
//                    $this->setState('service', $this->service->serviceName);

                    $this->errorCode = self::ERROR_NONE;
                }else{
                    $this->errorCode = self::ERROR_STATUS_INACTIVE;
                }
            }else{

                $this->errorCode = self::ERROR_EMAIL_NOT_EXIST;
            }

//			$this->setState('id', $this->id);
//			$this->setState('name', $this->name);
//			$this->setState('service', $this->service->serviceName);

			// You can save all given attributes in session.
			//$attributes = $this->service->getAttributes();
			//$session = Yii::app()->session;
			//$session['eauth_attributes'][$this->service->serviceName] = $attributes;


		}else{
			$this->errorCode = self::ERROR_NOT_AUTHENTICATED;
		}
		return !$this->errorCode;
	}

	/**
	 * Returns the unique identifier for the identity.
	 * This method is required by {@link IUserIdentity}.
	 *
	 * @return string the unique identifier for the identity.
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Returns the display name for the identity.
	 * This method is required by {@link IUserIdentity}.
	 *
	 * @return string the display name for the identity.
	 */
	public function getName() {
		return $this->name;
	}
}
