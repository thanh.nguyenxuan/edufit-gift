<?php

/**
 * Class ActiveRecord includes SoftDelete
 */
class ActiveRecord extends CActiveRecord
{
    CONST STATUS_ACTIVE = 1;
    CONST STATUS_INACTIVE = 0;

    public function getOriginalTableName()
    {
        return Yii::app()->db->tablePrefix.str_replace(array('{{','}}'),'',$this->tableName());
    }

    protected function beforeSave()
    {
        if($this->isNewRecord){
            if($this->hasAttribute('created_at')){
                $this->created_at = date('Y-m-d H:i:s');
            }
        }

        return TRUE;
    }


}
?>