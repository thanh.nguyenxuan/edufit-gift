<?php

return array(
	'Model Name'=>'Nazwa modelu',
	'Label field name'=>'Nazwa etykiety pola',
	'Empty item name'=>'Nazwa pustego elementu',
	'Profile model relation name'=>'Nazwa profilu modelu',
);
