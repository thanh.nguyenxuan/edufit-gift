<?php

return array(
	'Model Name'=>'Nome do modelo',
	'Label field name'=>'Nome do campo',
	'Empty item name'=>'Nome de item vazio',
	'Profile model relation name'=>'Modelo de perfil com relação ao nome',
);
