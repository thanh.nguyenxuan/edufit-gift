<?php

return array(
	'Model Name'=>'Имя модели',
	'Label field name'=>'Поле для отображения в списке',
	'Empty item name'=>'Текст пустого поля',
	'Profile model relation name'=>'Название связи (relation) модели профиля',
);
