<?php

class AController extends CController
{
    public function init()
    {
        Yii::app()->errorHandler->errorAction='api/error';
        $this->nginxAllowCORS();
        $this->preventXSS();
        $this->checkLanguage();
    }

    public function nginxAllowCORS()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        header("Access-Control-Allow-Headers: Authorization");
    }

    public function preventXSS()
    {
        if (isset($_GET) && count($_GET) > 0) {
            $p = new CHtmlPurifier();
            foreach ($_GET as $k => $v) {
                $_GET[$k] = $p->purify($v);
            }
        }

        if (isset($_POST) && count($_POST) > 0) {
            $p = new CHtmlPurifier();
            foreach ($_POST as $k => $v) {
                $_POST[$k] = $p->purify($v);
            }
        }
    }

    public function checkLanguage()
    {
        $header = getallheaders();
        if(!empty($header['language'])){
            $language = strtolower($header['language']);
            $accept_lang = array(
                'vi',
//                'en',
//                'jp'
            );
            if(in_array($language, $accept_lang)){
                Yii::app()->setLanguage($language);
            }
        }
    }
}
