<?php

/**
 * Class WPrize
 * @property WGift[] $gifts
 */
class WPrize extends Prize
{

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'gifts' => array(self::HAS_MANY, 'WGift', array('prize_id' => 'id')),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider | static[] the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($dataProvider = TRUE)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria();
        $criteria->with = array('gifts' => array('with' => array('gift_player' => array('with' => 'player'))));
        $criteria->order = 't.level ASC, t.sort ASC';

        if($dataProvider){
            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
            ));
        }else{
            return self::model()->findAll($criteria);
        }
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WPrize the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getListData()
    {
        $data = array();

        $criteria = new CDbCriteria();
        $criteria->with = array('gifts' => array('with' => array('gift_player' => array('with' => 'player'))));
        $criteria->order = 't.level ASC, t.sort ASC';
        $models = self::model()->findAll($criteria);

        if(!empty($models)){
            foreach ($models as $model){
                $data[] = $model->convertToDataJson();
            }
        }
        return $data;
    }

    public function convertToDataJson()
    {
        $gifts = array();
        if(!empty($this->gifts)){
            foreach ($this->gifts as $gift){
                $gifts[] = array(
                    "id"        => intval($gift->id),
                    "name"      => $gift->name,
                    "image"     => !empty($this->image) ? $this->image : '',
                    "image_disable" => !empty($this->image_disable) ? $this->image_disable : '',
                    "video"     => $gift->video,
                    "slot"      => intval($gift->slot),
                    "sort"      => intval($gift->sort),
                    "status"    => intval($gift->status),
                    "winner"   => !empty($gift->gift_player) ? array(
                        "id"        => intval($gift->gift_player->player->id),
                        "code"      => $gift->gift_player->player->code,
                        "name"      => $gift->gift_player->player->name,
                        "phone"     => $gift->gift_player->player->phone,
                        "email"     => $gift->gift_player->player->email,
                        "position"  => $gift->gift_player->player->position,
                        "department"=> $gift->gift_player->player->department,
                        "school"    => $gift->gift_player->player->school,
                    ) : array(
                        "id"        => '',
                        "code"      => '',
                        "name"      => '',
                        "phone"     => '',
                        "email"     => '',
                        "position"  => '',
                        "department"=> '',
                        "school"    => '',
                    )
                );
            }
        }
        $name = $this->name;
        $arr_name = explode('<br/>', $name);
        $name_vi = $arr_name[0];
        $name_en = isset($arr_name[1]) ? $arr_name[1] : '';

        return array(
            "id"        => intval($this->id),
            "name"      => $name_vi,
            "name_en"   => $name_en,
            "level"     => intval($this->level),
            "slot"      => intval($this->slot),
            "image"     => !empty($this->image) ? $this->image : '',
            "image_disable" => !empty($this->image_disable) ? $this->image_disable : '',
            "video"     => $this->video,
            "status"    => intval($this->status),
            "sort"      => intval($this->sort),
            "gifts"     => $gifts
        );
    }


    /**
     * Hàm xử lý quay giải
     *
     * @return array
     */
    public function spinThePrize()
    {
        if($this->status == self::STATUS_ACTIVE)                    // kiểm tra xem giải đã quay xong chưa
        {
            $players = WPlayer::getListDataAvailable();             // lấy danh sách người dùng để quay thưởng

            // loai CTV ra khoi giai 1,2,3,4
            if (in_array($this->id, [1, 2, 3, 4])) {
                $players = WPlayer::getListDataAvailable(true);
            }
            $winners = array();
            foreach ($this->gifts as $gift){                        // 1 giải gồm nhiều phần quà, quay từng quà 1
                if(empty($gift->gift_player)){                      // kiểm tra xem giải đã có người trúng chưa ( trường hợp ko lên nhận quà, quay lại)
                    if(empty($players)){                            // nếu danh sách người dùng không còn đủ -> dừng
                        break;
                    }
                    /**
                     * (index của mảng trong lập trình chạy từ 0 đến (length-1) ví dụ phần tử thứ nhất sẽ là array[0], phẩn từ cuối là array[count(array)-1]
                     */
                    $start = 0;                                     // chỉ số ngâu nhiên bắt đầu từ 0
                    $end = count($players) - 1;                     //chỉ số ngâu nhiên kết thúc đến số lượng nhân viên trừ 1
                    $index = rand($start, $end);                    // lấy ngẫu nhiên một index
                    $winners[$players[$index]->id] = $gift->id;     // chọn người trúng

                    // lấy thông tin người trúng giải
                    $name = $players[$index]->name;
                    $code = $players[$index]->code;
                    $phone = $players[$index]->phone;
                    $content = Utils::utf8convert("Chúc mừng $name - $code ".$this->convertName());

                    // gửi tin nhắn (đã bỏ)
                    /*$response = Utils::sendSms($phone,$content);

                    $log_sms = new LogSms();
                    $log_sms->target = $phone;
                    $log_sms->brand_name = 'EDUFIT';
                    $log_sms->content = $content;
                    $log_sms->send_at = date('Y-m-d H:i:s');
                    $log_sms->response = json_encode($response);
                    $log_sms->save();*/


                    //bỏ người đã trúng trong danh sách người dùng để tiếp tục quay phần quà tiếp theo
                    array_splice($players, $index, 1);
                }
            }


            // tổng hợp lại thông tin trúng giải gửi về cho web
            foreach ($winners as $player_id => $gift_id){
                $gift_player = new WGiftPlayer();
                $gift_player->player_id = $player_id;
                $gift_player->gift_id = $gift_id;
                $gift_player->status = WGiftPlayer::STATUS_ACTIVE;
                $gift_player->save();
            }

            // cập nhật lại trạng thái giải đã quay xong
            $this->status = self::STATUS_INACTIVE;
            $this->save();
        }

        $model = self::getById($this->id);
        $json_data = $model->convertToDataJson();
        return array('gifts' => $json_data['gifts']);
    }


    public static function getPrizeByGift($gift_id)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "t.id IN (SELECT prize_id FROM gift WHERE id = '$gift_id')";
        return self::model()->find($criteria);
    }

    public static function getById($id)
    {
        $criteria = new CDbCriteria();
        $criteria->with = array('gifts' => array('with' => array('gift_player' => array('with' => 'player'))));
        $criteria->compare('t.id', $id);
        $criteria->order = 'gifts.sort ASC';

        return self::model()->find($criteria);
    }

    public function convertName()
    {
	    return str_replace('<br/>', '/', $this->name);
    }

    public function getDescription()
    {
        $des = '';

//        switch ($this->id){
//            case 9: $des = '('.Yii::t('web/model/prize', '4th_time').')'; break;
//            case 10: $des = '('.Yii::t('web/model/prize', '3rd_time').')'; break;
//            case 11: $des = '('.Yii::t('web/model/prize', '2nd_time').')'; break;
//            case 12: $des = '('.Yii::t('web/model/prize', '1st_time').')'; break;
//            case 13: $des = '('.Yii::t('web/model/prize', '4th_time').')'; break;
//            case 14: $des = '('.Yii::t('web/model/prize', '3rd_time').')'; break;
//            case 15: $des = '('.Yii::t('web/model/prize', '2nd_time').')'; break;
//            case 16: $des = '('.Yii::t('web/model/prize', '1st_time').')'; break;
//            case 17: $des = '('.Yii::t('web/model/prize', '4th_time').')'; break;
//            case 18: $des = '('.Yii::t('web/model/prize', '3rd_time').')'; break;
//            case 19: $des = '('.Yii::t('web/model/prize', '2nd_time').')'; break;
//            case 20: $des = '('.Yii::t('web/model/prize', '1st_time').')'; break;
//        }
        return $des;
    }

}
