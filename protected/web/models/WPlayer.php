<?php

class WPlayer extends Player
{

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider | static[]
     * based on the search/filter conditions.
     */
    public function search($dataProvider = TRUE)
    {
        $criteria = new CDbCriteria;

        $criteria->compare('code', $this->code, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('position', $this->position, true);
        $criteria->compare('department', $this->department, true);
        $criteria->compare('school', $this->school, true);

        $criteria->order = 'lastname ASC, firstname ASC';

        if ($dataProvider) {
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        } else {
            return self::model()->findAll($criteria);
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return WPlayer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Lấy danh sách người dùng để quay trúng thưởng
     *
     * @return array|mixed|null
     */
    public static function getListDataAvailable($excludeCTV = false)
    {
        $criteria = new CDbCriteria();
//        if (!$excludeCTV) {
//            $criteria->condition = "t.id NOT IN (SELECT player_id FROM gift_player) AND t.code IS NOT NULL";
//        } else {
//            $criteria->condition = "t.id NOT IN (SELECT player_id FROM gift_player) AND t.code IS NOT NULL AND t.code NOT IN (11811012,
//11904018,
//12101024,
//12203036,
//12203140,
//12205005,
//12209076,
//12209084,
//12209088,
//12209129,
//12210019,
//12210020,
//12210022,
//12210024,
//12211027,
//12211045,
//92212001,
//92212002,
//92212003,
//12212024,
//11808061,
//12203023,
//12012047,
//12207143)";
//        }
        if (!$excludeCTV) {
            $criteria->condition = "t.id NOT IN (SELECT player_id FROM gift_player)";
        } else {
            $criteria->condition = "t.id NOT IN (SELECT player_id FROM gift_player)";
        }

        $criteria->compare('t.status', self::STATUS_ACTIVE);
        return self::model()->findAll($criteria);
    }
}
