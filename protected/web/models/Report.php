<?php

class Report extends CFormModel
{

    /**
     * @return WPrize[]
     */
    public static function reportWinners()
    {
        $criteria = new CDbCriteria();
        $criteria->with = array('gifts' => array('with' => array('gift_player' => array('with' => 'player'))));
        $criteria->order = 't.sort ASC, gifts.sort ASC';
        return WPrize::model()->findAll($criteria);
    }

    public static function exportWinners()
    {
        ini_set('max_execution_time', 60*10);
        ini_set('memory_limit', '-1');
        Yii::import('ext.yexcel.Classes.PHPExcel');

        $data = self::reportWinners();

        $file_name = 'Danh sách trúng thưởng Edufit LuckyDraw '.date("YmdHis").'.xlsx';

        $excel = new PHPExcel();
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=$file_name");
        header("Cache-Control: max-age=0");
        $i=1;
        $excel->setActiveSheetIndex(0)->setTitle('Winners')
            ->setCellValue("A$i", 'Ảnh giải thưởng')
            ->setCellValue("B$i", 'Tên giải thưởng')
            ->setCellValue("C$i", 'Tên sản phẩm')
            ->setCellValue("D$i", 'Giá trị (VND)')
            ->setCellValue("E$i", 'Mã người trúng')
            ->setCellValue("F$i", 'Tên người trúng')
            ->setCellValue("G$i", 'Email')
            ->setCellValue("H$i", 'Vị trí')
            ->setCellValue("I$i", 'Phòng ban')
        ;

        foreach ($data as $prize){
            $row_span = count($prize->gifts);
            $i++;
            $row_open = TRUE;
            $excel->getActiveSheet()->setCellValue("B$i", $prize->name)
                ->mergeCells("A$i:A".($i+$row_span-1))
                ->mergeCells("B$i:B".($i+$row_span-1))
            ;
            $total_gift = count($prize->gifts);
            if(!empty($prize->image)){
                $avatar = realpath(Yii::app()->basePath.'/../'.$prize->image);
                if($avatar){
                    $objDrawing = new PHPExcel_Worksheet_Drawing();
                    $objDrawing->setPath($avatar);
                    $objDrawing->setCoordinates("A$i");
                    $objDrawing->setHeight(100);
                    $objDrawing->setWorksheet($excel->getActiveSheet());
                    if(count($prize->gifts) < 8){
                        $row_height = ceil(100/$total_gift);
                        for($k=$i; $k<($i+$row_span); $k++){
                            $excel->getActiveSheet()->getRowDimension($k)->setRowHeight($row_height);
                        }
                    }
                    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(24);
                }
            }
            foreach ($prize->gifts as $gift){
                if(!$row_open){
                    $i++;
                }
                $excel->getActiveSheet()
                    ->setCellValue("C$i", $gift->name)
                    ->setCellValueExplicit("D$i", Utils::formatCurrency($gift->price, ''))
                    ->setCellValue("E$i", (!empty($gift->gift_player) ? $gift->gift_player->player->code : ''))
                    ->setCellValue("F$i", (!empty($gift->gift_player) ? $gift->gift_player->player->name : ''))
                    ->setCellValue("G$i", (!empty($gift->gift_player) ? $gift->gift_player->player->email : ''))
                    ->setCellValue("H$i", (!empty($gift->gift_player) ? $gift->gift_player->player->position : ''))
                    ->setCellValue("I$i", (!empty($gift->gift_player) ? $gift->gift_player->player->department : ''))
                    ;
                $row_open = FALSE;
            }
        }

        $excel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
        $excel->getActiveSheet()->getStyle("D2:D$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $excel->getActiveSheet()->getStyle("A1:I$i")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
        foreach(range('B','I') as $columnID) {
            $excel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $writer->save('php://output');

    }


}
