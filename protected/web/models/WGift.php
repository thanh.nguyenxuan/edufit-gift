<?php

/**
 * Class WGift
 * @property WGiftPlayer $gift_player
 */
class WGift extends Gift
{
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'gift_player' => array(self::HAS_ONE, 'WGiftPlayer', array('gift_id' => 'id'),
                'on' => 'gift_player.status = '.self::STATUS_ACTIVE
            ),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return WGift the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getWinnerName()
    {
        $winner = $this->gift_player->player;
        return $winner->name.' - '.$winner->code;
    }

    public function getWinnerDepartment()
    {
        $winner = $this->gift_player->player;
//        return $winner->position.' - '.$winner->department;
        return $winner->department;
    }


}
