<?php

class SiteController extends WController
{

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);


		}
	}

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->render('index');
    }


    public function actionListPrize()
    {
        $this->render('list_prize');
    }

    public function actionPrize($id)
    {
        $model = WPrize::getById($id);

        $this->render('prize', [
            'model' => $model
        ]);
    }

    public function actionReport()
    {
        $data = Report::reportWinners();

        $this->render('report',[
            'data' => $data
        ]);
    }

    public function actionExportWinner()
    {
        Report::exportWinners();
    }

}