<?php

class ApiController extends AController
{

    public function init()
    {
        parent::init();

        if(!$GLOBALS['config_common']['api']['active']){
            $return = array(
                'success' => false,
                'message' => 'API Port has been closed',
                'data' => array()
            );

            $response = CJSON::encode($return);

            $httpVersion = Yii::app()->request->getHttpVersion();
            header("HTTP/$httpVersion 403 Forbidden");
            header('Content-Type: application/json');
            echo $response;
            YIi::app()->end(403);
        }
    }

    public function actionLogin()
    {
        $return = array(
            'success' => true,
            'message' => '',
            'data' => array()
        );

        $type = __CLASS__.'_'.__FUNCTION__;
        $url = Yii::app()->urlManager->parseUrl(Yii::app()->request);
        $method = $_SERVER['REQUEST_METHOD'];
        $arr_params = array(
            'header' => apache_request_headers(),
            'params' => $_REQUEST
        );
        $http_status = 200;

        if(Yii::app()->request->isPostRequest){

        }else{
            $return['success'] = false;
            $return['message'] = Yii::t('web/error', 'wrong method request');
        }

        $response = CJSON::encode($return);
        Utils::writeLog($type, $url, $method, $arr_params, $http_status, $response);

        $httpVersion = Yii::app()->request->getHttpVersion();
        header("HTTP/$httpVersion 200 OK");
        header('Content-Type: application/json');
        echo $response;
        YIi::app()->end($http_status);
    }

    protected function authorization()
    {
        return TRUE;

        $return = array(
            'success' => false,
            'message' => '',
        );
        $token = $this->getToken();
        if(!empty($token)){

        }else{
            $return['message'] = Yii::t('web/error', 'Authorization Failed!') . ' Token empty';
        }
        $httpVersion = Yii::app()->request->getHttpVersion();
        header("HTTP/$httpVersion 403 Forbidden");
        header('Content-Type: application/json');
        $response = CJSON::encode($return);
        echo $response;
        Yii::app()->end();
    }

    protected function getToken()
    {
        $token = null;
        $header = getallheaders();
        if(is_array($header)){
            $header = array_change_key_case($header, CASE_LOWER);
        }
        if(isset($header['authorization']) && !empty($header['authorization'])){
            $authorization = $header['authorization'];
            $arrToken = explode(' ', $authorization);
            $token = $arrToken[1];
        }
        return $token;
    }

    public function actionError()
    {
        $error=Yii::app()->errorHandler->error;
        echo Yii::t('api', 'Error Message'). ': '. CHtml::encode($error['message']);
    }

    public function actionCommon()
    {
        $this->authorization();

        $return = array(
            'success' => true,
            'message' => '',
            'data' => array()
        );

        $type = __CLASS__.'_'.__FUNCTION__;
        $url = Yii::app()->urlManager->parseUrl(Yii::app()->request);
        $method = $_SERVER['REQUEST_METHOD'];
        $arr_params = array(
            'header' => apache_request_headers(),
            'params' => $_REQUEST
        );
        $http_status = 200;

        $return['data']['total_user'] = WPlayer::model()->count();
        $return['data']['total_gift'] = WGift::model()->count();

        $response = CJSON::encode($return);
        Utils::writeLog($type, $url, $method, $arr_params, $http_status, $response);

        $httpVersion = Yii::app()->request->getHttpVersion();
        header("HTTP/$httpVersion 200 OK");
        header('Content-Type: application/json');
        echo $response;
        YIi::app()->end($http_status);
    }

    public function actionListUser()
    {
        $this->authorization();

        $return = array(
            'success' => true,
            'message' => '',
            'data' => array()
        );

        $type = __CLASS__.'_'.__FUNCTION__;
        $url = Yii::app()->urlManager->parseUrl(Yii::app()->request);
        $method = $_SERVER['REQUEST_METHOD'];
        $arr_params = array(
            'header' => apache_request_headers(),
            'params' => $_REQUEST
        );
        $http_status = 200;

        $model = new WPlayer('search');
        if(!empty($_GET['name'])){       $model->name = $_GET['name'];}
        if(!empty($_GET['email'])){      $model->email = $_GET['email'];}
        if(!empty($_GET['code'])){       $model->code = $_GET['code'];}
        if(!empty($_GET['phone'])){      $model->phone = $_GET['phone'];}
        if(!empty($_GET['position'])){   $model->position = $_GET['position'];}
        if(!empty($_GET['department'])){ $model->department = $_GET['department'];}
        if(!empty($_GET['school'])){     $model->school = $_GET['school'];}

        $players = $model->search(FALSE);
        foreach ($players as $player){
            $return['data'][] = array(
                'id'        => intval($player->id),
                'code'      => $player->code,
                'name'      => $player->name,
                'phone'     => $player->phone,
                'email'     => $player->email,
                'position'  => $player->position,
                'department'=> $player->department,
                'school'    => $player->school,
            );
        }

        $response = CJSON::encode($return);
        Utils::writeLog($type, $url, $method, $arr_params, $http_status, $response);

        $httpVersion = Yii::app()->request->getHttpVersion();
        header("HTTP/$httpVersion 200 OK");
        header('Content-Type: application/json');
        echo $response;
        YIi::app()->end($http_status);
    }


    public function actionUser()
    {
        $this->authorization();

        $return = array(
            'success' => true,
            'message' => '',
            'data' => array()
        );

        $type = __CLASS__.'_'.__FUNCTION__;
        $url = Yii::app()->urlManager->parseUrl(Yii::app()->request);
        $method = $_SERVER['REQUEST_METHOD'];
        $arr_params = array(
            'header' => apache_request_headers(),
            'params' => $_REQUEST
        );
        $http_status = 200;

        if(isset($_GET['id'])){
            $player = WPlayer::model()->findByPk($_GET['id']);
            if($player){
                $return['data'] = array(
                    'id'        => intval($player->id),
                    'code'      => $player->code,
                    'name'      => $player->name,
                    'phone'     => $player->phone,
                    'email'     => $player->email,
                    'position'  => $player->position,
                    'department'=> $player->department,
                    'school'    => $player->school,
                );
            }
        }else{
            $return['success'] = FALSE;
            $return['message'] = Yii::t('web/error', 'missing data request');
        }

        $response = CJSON::encode($return);
        Utils::writeLog($type, $url, $method, $arr_params, $http_status, $response);

        $httpVersion = Yii::app()->request->getHttpVersion();
        header("HTTP/$httpVersion 200 OK");
        header('Content-Type: application/json');
        echo $response;
        YIi::app()->end($http_status);
    }

    public function actionListPrize()
    {
        $this->authorization();

        $return = array(
            'success' => true,
            'message' => '',
            'data' => array()
        );

        $type = __CLASS__.'_'.__FUNCTION__;
        $url = Yii::app()->urlManager->parseUrl(Yii::app()->request);
        $method = $_SERVER['REQUEST_METHOD'];
        $arr_params = array(
            'header' => apache_request_headers(),
            'params' => $_REQUEST
        );
        $http_status = 200;

        $return['data'] = WPrize::getListData();

        $response = CJSON::encode($return);
        Utils::writeLog($type, $url, $method, $arr_params, $http_status, $response);

        $httpVersion = Yii::app()->request->getHttpVersion();
        header("HTTP/$httpVersion 200 OK");
        header('Content-Type: application/json');
        echo $response;
        YIi::app()->end($http_status);
    }


    public function actionPrize()
    {
        $this->authorization();

        $return = array(
            'success' => true,
            'message' => '',
            'data' => array()
        );


        $type = __CLASS__.'_'.__FUNCTION__;
        $url = Yii::app()->urlManager->parseUrl(Yii::app()->request);
        $method = $_SERVER['REQUEST_METHOD'];
        $arr_params = array(
            'header' => apache_request_headers(),
            'params' => $_REQUEST
        );
        $http_status = 200;

        if(isset($_GET['id'])){
            $prize = WPrize::getById($_GET['id']);
            if($prize){
                $return['data'] = $prize->convertToDataJson();
            }else{
                $return['message'] = Yii::t('web/error', 'data not found');
            }
        }else{
            $return['success'] = FALSE;
            $return['message'] = Yii::t('web/error', 'missing data request');
        }

        $response = CJSON::encode($return);
        Utils::writeLog($type, $url, $method, $arr_params, $http_status, $response);

        $httpVersion = Yii::app()->request->getHttpVersion();
        header("HTTP/$httpVersion 200 OK");
        header('Content-Type: application/json');
        echo $response;
        YIi::app()->end($http_status);
    }

    public function actionSpinThePrize()
    {
        $return = array(
            'success' => true,
            'message' => '',
            'data' => array()
        );

        $type = __CLASS__.'_'.__FUNCTION__;
        $url = Yii::app()->urlManager->parseUrl(Yii::app()->request);
        $method = $_SERVER['REQUEST_METHOD'];
        $arr_params = array(
            'header' => apache_request_headers(),
            'params' => $_REQUEST
        );
        $http_status = 200;

        if(Yii::app()->request->isPostRequest){
            if(!empty($_POST['prize_id'])){
                $prize = WPrize::getById($_POST['prize_id']);
                if($prize){
                    if($prize->status == WGift::STATUS_ACTIVE){
                        $return['data'] = $prize->spinThePrize();
                    }else{
                        $return['message'] = Yii::t('web/model/prize', 'prize inactive');
                    }
                }else{
                    $return['success'] = FALSE;
                    $return['message'] = Yii::t('web/error', 'data not found');
                }
            }else{
                $return['success'] = FALSE;
                $return['message'] = Yii::t('web/error', 'missing data request');
            }
        }else{
            $return['success'] = FALSE;
            $return['message'] = Yii::t('web/error', 'wrong method request');
        }

        $response = CJSON::encode($return);
        Utils::writeLog($type, $url, $method, $arr_params, $http_status, $response);

        $httpVersion = Yii::app()->request->getHttpVersion();
        header("HTTP/$httpVersion 200 OK");
        header('Content-Type: application/json');
        echo $response;
        YIi::app()->end($http_status);
    }

    public function actionUserNotReceivePrize()
    {
        $return = array(
            'success' => true,
            'message' => '',
            'data' => array()
        );

        $type = __CLASS__.'_'.__FUNCTION__;
        $url = Yii::app()->urlManager->parseUrl(Yii::app()->request);
        $method = $_SERVER['REQUEST_METHOD'];
        $arr_params = array(
            'header' => apache_request_headers(),
            'params' => $_REQUEST
        );
        $http_status = 200;

        if(Yii::app()->request->isPostRequest){
            if(isset($_POST['user_id'])){
                $gift_player = WGiftPlayer::model()->findByAttributes(array('player_id' => $_POST['user_id']));
                if($gift_player){
                    $gift_player->status = WGiftPlayer::STATUS_INACTIVE;
                    $gift_player->save();

                    $prize = WPrize::getPrizeByGift($gift_player->gift_id);
                    $prize->status = WPrize::STATUS_ACTIVE;
                    $prize->save();
                }else{
                    $return['success'] = FALSE;
                    $return['message'] = Yii::t('web/error', 'data not found');
                }
            }else{
                $return['success'] = FALSE;
                $return['message'] = Yii::t('web/error', 'missing data request');
            }
        }else{
            $return['success'] = FALSE;
            $return['message'] = Yii::t('web/error', 'wrong method request');
        }

        $response = CJSON::encode($return);
        Utils::writeLog($type, $url, $method, $arr_params, $http_status, $response);

        $httpVersion = Yii::app()->request->getHttpVersion();
        header("HTTP/$httpVersion 200 OK");
        header('Content-Type: application/json');
        echo $response;
        YIi::app()->end($http_status);
    }


    public function actionReset()
    {
        $return = array(
            'success' => true,
            'message' => '',
            'data' => array()
        );

        $type = __CLASS__.'_'.__FUNCTION__;
        $url = Yii::app()->urlManager->parseUrl(Yii::app()->request);
        $method = $_SERVER['REQUEST_METHOD'];
        $arr_params = array(
            'header' => apache_request_headers(),
            'params' => $_REQUEST
        );
        $http_status = 200;

        if(Yii::app()->request->isPostRequest){
            WPrize::model()->updateAll(array('status' => WPrize::STATUS_ACTIVE));
            WGiftPlayer::model()->deleteAll();
        }else{
            $return['success'] = FALSE;
            $return['message'] = Yii::t('web/error', 'wrong method request');
        }

        $response = CJSON::encode($return);
        Utils::writeLog($type, $url, $method, $arr_params, $http_status, $response);

        $httpVersion = Yii::app()->request->getHttpVersion();
        header("HTTP/$httpVersion 200 OK");
        header('Content-Type: application/json');
        echo $response;
        YIi::app()->end($http_status);
    }

}