<?php

/**
 * This is the model class for table "log_sms".
 *
 * The followings are the available columns in table 'log_sms':
 * @property integer $id
 * @property string $agent
 * @property string $brand_name
 * @property string $target
 * @property string $content
 * @property integer $status
 * @property integer $created_by
 * @property string $created_at
 * @property integer $sent
 * @property string $send_at
 * @property string $cancel_at
 * @property integer $schedule_id
 * @property string $type
 * @property integer $pid
 * @property string $request
 * @property string $response
 * @property string $student_code
 */
class LogSms extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log_sms';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, created_by, sent, schedule_id, pid', 'numerical', 'integerOnly'=>true),
			array('agent, target, type, student_code', 'length', 'max'=>255),
			array('brand_name, content, created_at, send_at, cancel_at, request, response', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, agent, brand_name, target, content, status, created_by, created_at, sent, send_at, cancel_at, schedule_id, type, pid, request, response, student_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'agent' => 'Agent',
			'brand_name' => 'Brand Name',
			'target' => 'Target',
			'content' => 'Content',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_at' => 'Created At',
			'sent' => 'Sent',
			'send_at' => 'Send At',
			'cancel_at' => 'Cancel At',
			'schedule_id' => 'Schedule',
			'type' => 'Type',
			'pid' => 'Pid',
			'request' => 'Request',
			'response' => 'Response',
			'student_code' => 'Student Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('agent',$this->agent,true);
		$criteria->compare('brand_name',$this->brand_name,true);
		$criteria->compare('target',$this->target,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('sent',$this->sent);
		$criteria->compare('send_at',$this->send_at,true);
		$criteria->compare('cancel_at',$this->cancel_at,true);
		$criteria->compare('schedule_id',$this->schedule_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('pid',$this->pid);
		$criteria->compare('request',$this->request,true);
		$criteria->compare('response',$this->response,true);
		$criteria->compare('student_code',$this->student_code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LogSms the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
